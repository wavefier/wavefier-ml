from sklearn import preprocessing
from numpy import array

class Scaling:
    """
    This class contains set of static methods responsible for the preprocessing of the data required by ML algorithms
    """
    @staticmethod
    def normalize(data: array):
        """
        This method rescales the data in the range from 0 to 1 (based on sklearn)

        :type data: numpy.array
        :param data: Numpy array containing the data to be normalized
        :return: Normalized numpy array
        """
        data_norm = preprocessing.minmax_scale(data.T).T
        return data_norm
