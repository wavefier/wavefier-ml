from wavefier_common.Trigger import Trigger
from wavefier_ml.algorithms.autoencoder.Prediction import Prediction as AE_prediction
from wavefier_ml.algorithms.cnn1d.Prediction import Prediction as CNN_prediction

import logging

class TriggerAnalysis:
    """
    This class is responsible for the communication with the chosen algorithm to process data
    """
    def __init__(self, trigger: Trigger, mode: str):
        """
        This class is responsible for the communication with the chosen algorithm to process data

        :type triggers: TriggerCollection
        :param triggers: TriggerCollection object containing set of triggers to analyse

        :type mode: str
        :param mode: Parameter specifying type of analysis

        """
        self.trigger = trigger
        self.mode = mode


    def run(self):

        """
        Method calling chosen algorithm to process data - Trigger object

        :return:
        """

        logging.info("Running analysis in the mode %s"%self.mode)
        if self.mode == "cluster":
            # run clustering algorithm
            prediction = AE_prediction.predict(self.trigger)
        elif self.mode == "classify":
            # run classifying algorithm
            prediction = CNN_prediction.predict(self.trigger)
        else:
            logging.error("MLA type not handled")
            prediction = None
        return prediction

    def run_all(self):

        """
        Deprecated method
        Method calling chosen algorithm to process data - Trigger Collection object

        :return:
        """

        logging.info("Running analysis in the mode %s"%self.mode)
        if self.mode == "cluster":
            # run clustering algorithm
            prediction = AE_prediction.predict_all(self.triggers)
        elif self.mode == "classify":
            # run classifying algorithm
            prediction = CNN_prediction.predict_all(self.triggers)
        else:
            logging.error("MLA type not handled")
            prediction = None
        return prediction
