from influxdb import InfluxDBClient
import sys
import logging


import wavefier_common
from time import sleep

from wavefier_common.util.Time import Time as TimeConverter
from wavefier_common.Trigger import Trigger
from wavefier_common.TriggerUnsupervisedResults import TriggerUnsupervisedResults
from wavefier_common.TriggerSupervisedResults import TriggerSupervisedResults


class MLResults2DBImporter(object):
    """This class is responsible for inserting a MLResults object into the InfluxDB database

    Args:
        **hostname** the hostname where is InfluxDB
        **port** the port where is InfluxDB
        **database_name** The database Name
        **logger** A custom Logger
    """

    def __init__(self, hostname: str, port: int, database_name: str, logger=None) -> None:
        self.logger = logger or logging.getLogger(__name__)
        self._connectionHost = hostname
        self._connectionPort = port
        self._connectionDatabase = database_name

    @staticmethod
    def make_event(trigger: Trigger,  result: TriggerUnsupervisedResults):
        algoritmName = next(iter(result.algorithm_config))
        algoritmValue = result.ml_results[algoritmName]
        time_hreadable = TimeConverter.gps2isoformat(trigger.getGPS())

        return [{
            "measurement": algoritmName,
            "time": time_hreadable,
            "tags": {
                "tag_trigger_uuid": result.uuid
            },
            "fields": {
                "trigger_uuid": result.uuid,
                "time": time_hreadable,
                "gps": trigger.getGPS(),
                "value": algoritmValue,
                "snr": trigger.getSNR(),
                "freq": trigger.getFreq()
            }
        }]

    def _import(self, trigger, result) -> bool:
        out = True
        try:
            self.logger.debug('A - Open InfluxDB connection')
            client = InfluxDBClient( self._connectionHost, self._connectionPort, '', '', self._connectionDatabase)

            self.logger.debug('B - Generate Message... ')
            json_body = self.make_event(trigger, result)
            
            self.logger.debug('C - Insert message: ' + str(json_body))
            client.write_points(json_body)

            self.logger.debug('D - Close InfluxDB')
            client.close()
            
        except:
            out = False
            self.logger.exception("Error on save MlResults on Database")

        return out

    def newResultsAppears(self, trigger: Trigger,  result: object, retry: int = 5) -> bool:
        done = False
    
        if result is not None and (isinstance(result, TriggerUnsupervisedResults) or isinstance(result, TriggerSupervisedResults)  ) :

            cretry = 0
            while not done and cretry <= retry:
                self.logger.debug("New MLResult to save on DB")
                done = self._import(trigger, result)
                if not done:
                    cretry = cretry + 1
                    sleep(0.1)
                    logging.info("... Some error, try one more time...")
        else:
            self.logger.warning("Result not recognised, IGNORED!")

        return done
