from wavefier_common.TriggerCollection import TriggerCollection
from wavefier_ml.algorithms.autoencoder.Model import Model as AE
from wavefier_ml.algorithms.cnn1d.Model import Model as CNN1D

import logging
logging.getLogger().setLevel(logging.INFO)

class TrainModel:
    """
    This class is responsible for the training of the chosen algorithm on the dataset
    """
    def __init__(self, triggers: TriggerCollection, mode: str = "cluster"):
        """
    This class is responsible for the training of the chosen algorithm on the dataset

        :type triggers: TriggerCollection
        :param triggers: TriggerCollection object containing set of triggers to analyse

        :type mode: str
        :param mode: Parameter specifying type of analysis

        """
        self.triggers = triggers
        self.mode = mode


    def run(self):

        """
        Method calling chosen algorithm to learn from the data

        :return:
        """

        logging.info("Running analysis in the mode %s"%self.mode)
        if self.mode == "cluster":
            # run clustering algorithm
            ae = AE(self.triggers)
            ae.fit()
        elif self.mode == "classify":
            # run classifying algorithm
            cnn1d = CNN1D(self.triggers)
            cnn1d.fit()

