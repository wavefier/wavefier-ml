import logging

from wavefier_common.Trigger import Trigger
from wavefier_trigger_handlers.AbstractTriggerListener import \
    AbstractTriggerListener

from wavefier_ml.RunMLAnalysis import RunMLAnalysis

logger = logging.getLogger(__name__)

class MLListener(AbstractTriggerListener):

    def newTriggerAppears(self, trigger: Trigger):
        """
        Executed whenever a Trigger is present

        :type trigger: Trigger
        :param trigger: Trigger object containing wavefier-wdf output
        :return:
        """
        if trigger is not None and isinstance(trigger, Trigger):
            mla1 = RunMLAnalysis(trigger, mode="cluster")
            mla1.run()

            mla2 = RunMLAnalysis(trigger, mode="classify")
            mla2.run()
