from wavefier_common.util.Path import Path
from wavefier_common.TriggerCollection import TriggerCollection
from wavefier_common.Trigger import Trigger
from wavefier_common.TriggerSupervisedResults import TriggerSupervisedResults
from wavefier_ml.preprocessing.Scaling import Scaling

from keras.models import model_from_json

import numpy as np
import os
import json
import logging

class Prediction:

    """
    This class uses precomputed 1 Dimensional Convolutional Neural Networks to classify the data
    """

    @staticmethod
    def predict(trigger: Trigger):
        """
        Static method classying single trigger

        :type trigger: Trigger
        :param trigger: Single Trigger object
        :return: Trigger Classify object with model configuration and the label denoting class of the trigger
        """
        logging.info("Not implemented")
        script_dir = os.path.abspath(os.path.dirname(__file__))
        subdir = os.path.join(script_dir, 'models')
        json_path = str(Path(subdir) / 'model.json')
        hdf_path = str(Path(subdir) / 'model.hdf5')

        # load json and create model
        json_file = open(json_path, 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        loaded_model = model_from_json(loaded_model_json)
        # load weights into new model
        loaded_model.load_weights(hdf_path)

        logging.info("Running the CNN 1D")
        coefficients = np.zeros((1, trigger.getNcoef()), dtype=float)
        for i in range(trigger.getNcoef()):
            coefficients[0, i] = trigger.getRW(i)
        normed_coefficients = Scaling.normalize(coefficients)
        normed_coefficients = normed_coefficients.reshape(normed_coefficients.shape[0], normed_coefficients.shape[1], 1)
        classified_data = loaded_model.predict(normed_coefficients).argmax(1)[0]
        logging.info("Data processed")

        # Store the results as Trigger Classify object
        trigger_classifier = TriggerSupervisedResults(trigger.uuid)
        # Load algorithm configuration
        logging.info("Loading configuration")
        script_dir = os.path.abspath(os.path.dirname(__file__))
        config_json = str(Path(script_dir) / 'config.json')
        with open(config_json, "r") as f:
            config = json.load(f)
        trigger_classifier.setAlgorithmConfig("cnn1d", config)
        trigger_classifier.setMLResults("cnn1d", classified_data)
        return trigger_classifier

    @staticmethod
    def predict_all(triggers: TriggerCollection):
        """
        Deprecated method
        Static method classying set of triggers

        :type trigger: TriggerCollection
        :param trigger: Set of Triggers
        :return: List of Trigger Classify objects with model configuration and the label denoting class of the trigger
        """
        logging.info("Not implemented")
        script_dir = os.path.abspath(os.path.dirname(__file__))
        subdir = os.path.join(script_dir, 'models')
        json_path = str(Path(subdir) / 'model.json')
        hdf_path = str(Path(subdir) / 'model.hdf5')

        # load json and create model
        json_file = open(json_path, 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        loaded_model = model_from_json(loaded_model_json)
        # load weights into new model
        loaded_model.load_weights(hdf_path)
        logging.info("Model loaded from disk")

        # Load algorithm configuration
        logging.info("Loading configuration")
        script_dir = os.path.abspath(os.path.dirname(__file__))
        config_json = str(Path(script_dir) / 'config.json')
        with open(config_json, "r") as f:
            config = json.load(f)

        logging.info("Running the CNN 1D")
        classified_all_triggers = []
        for trigger in triggers.iterator():
            coefficients = np.zeros((1, trigger.getNcoef()), dtype=float)
            for i in range(trigger.getNcoef()):
                coefficients[0, i] = trigger.getRW(i)
            normed_coefficients = Scaling.normalize(coefficients)
            normed_coefficients = normed_coefficients.reshape(normed_coefficients.shape[0], normed_coefficients.shape[1], 1)
            classified_data = loaded_model.predict(normed_coefficients).argmax(1)[0]

            # Store the results as Trigger Classify object
            trigger_classifier = TriggerSupervisedResults(trigger.uuid)
            trigger_classifier.setAlgorithmConfig("cnn1d", config)
            trigger_classifier.setMLResults("cnn1d", classified_data)
            classified_all_triggers.append(classified_data)

        logging.info("Data processed")
        return classified_all_triggers

