from tensorflow.keras.layers import Input, Dense, Dropout, Conv1D, MaxPool1D, Flatten
from tensorflow.keras.models import Model
from tensorflow.keras import optimizers

class DenseLayer:
    """
    The DenseLayer class is responsible for the initialization of fully connected layers in the CNN1D
    """
    def __init__(self, n_neurons: int, f: str, dropout: float):
        """
        The DenseLayer class is responsible for the initialization of fully connected layers in the CNN1D

        :type n_neurons: int
        :param n_neurons: Integer specifying the number of neurons in the dense layer.

        :type f: string
        :param f: Name of the activation function for the layer.

        :type dropout: float
        :param dropout: The value of dropout - fraction of neurons to be turned of during the training. If dropout=0.0
        then no Dropout is applied to the layer.
        """
        self.f = f
        self.n_neurons = n_neurons
        self.dropout = dropout

    def forward(self, X: Input):
        """
        The forward module initialize the layer with respect to the given set of parameters. If available,
        it applies dropout.

        :type X: keras layer
        :param X: The keras layer object passed from the previous layers or the input layer.
        """
        output = Dense(self.n_neurons, activation=self.f)(X)
        if self.dropout > 0.0:
            output = Dropout(self.dropout)(output)
        return output


class ConvLayer:
    """
    The ConvLayer class is responsible for the initialization of Convolutional layers in the CNN1D
    """
    def __init__(self, n_kernels: int, kernel_size: int, f: str, dropout: float, pool_size: int):
        """
        The ConvLayer class is responsible for the initialization of Convolutional layers in the CNN1D

        :type n_kernels: int
        :param n_kernels: Integer specifying the number of kernels in the convolutional layer

        :type kernel_size: int
        :param kernel_size: Size of the kernel in the convolutional layer

        :type f: string
        :param f: Name of the activation function for the layer.

        :type dropout: float
        :param dropout: The value of dropout - fraction of neurons to be turned of during the training. If dropout=0.0
        then no Dropout is applied to the layer.

        :type pool_size: int
        :param pool_size: Size of the pooling applied after convolutional layer. If pool_size = 0, then no Pooling is applied.
        """
        self.f = f
        self.n_kernels = n_kernels
        self.kernel_size = kernel_size
        self.pool_size = pool_size
        self.dropout = dropout

    def forward(self, X: Input):
        """
        The forward module initialize the layer with respect to the given set of parameters. If available,
        it applies dropout.

        :type X: keras layer
        :param X: The keras layer object passed from the previous layers or the input layer.
        """
        output = Conv1D(filters = self.n_kernels, kernel_size = self.kernel_size, activation=self.f)(X)
        if self.pool_size > 0:
            output = MaxPool1D(self.pool_size)(output)
        if self.dropout > 0.0:
            output = Dropout(self.dropout)(output)
        return output


class Architecture:
    """
    The Architecture class builds the Convolutional Neural Network 1D (CNN1D) based on the given structure and hyperparameters.
    """
    def __init__(self, data_shape: list, layers: dict, loss: dict, n_classes: int):
        """
        The Architecture class builds the Convolutional Neural Network 1D based on the given structure and hyperparameters.

        :type data_shape: list
        :param data_shape: Shape of the data to be analysed [n_samples, n_features, n_channels]

        :type layers: dict
        :param layers: Dictionary containing lists of parameters for each layer of the CNN 1D

        :type loss: str
        :param loss: Name of the loss function
        """
        self.dense_layers = layers["dense"]
        self.conv_layers = layers["convolution"]
        self.n_classes = n_classes
        self.lr = loss["lr"]
        self.optimizer = loss["optimizer"]
        self.loss = loss["cost"]
        self.inputs = Input(shape=(data_shape[1], data_shape[2]))

    def build(self):
        """
        The build module is responsible for the construction of the whole architecture for the Autoencoder based on
        the configuration file.
        """
        # Convolutional Part of CNN
        conv_layers = []
        for n_kernels, kernel_size, activ, dropout, pool_size in self.conv_layers:
            h = ConvLayer(n_kernels, kernel_size, activ, dropout, pool_size)
            conv_layers.append(h)

        # Process the Convolutional Part
        current_layer_value = self.inputs
        for layer in conv_layers:
            current_layer_value = layer.forward(current_layer_value)

        # Flatten
        current_layer_value = Flatten()(current_layer_value)

        # Fully connected part of CNN
        dense_layers = []
        for n_neurons, activ, dropout in self.dense_layers:
            h = DenseLayer(n_neurons, activ, dropout)
            dense_layers.append(h)

        # Process the Fully Connected Part
        for layer in dense_layers:
            current_layer_value = layer.forward(current_layer_value)

        final_layer = Dense(self.n_classes, activation="softmax")(current_layer_value)

        # Optimizer initialization
        opt_to_use = getattr(optimizers, self.optimizer)
        optimizer = opt_to_use(learning_rate=self.lr)

        # Create containers for all models
        self.classifier = Model(self.inputs, final_layer)

        # Compile the model
        self.classifier.compile(loss=self.loss, optimizer=optimizer)
