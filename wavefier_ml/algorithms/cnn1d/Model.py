from wavefier_ml.algorithms.cnn1d.Architecture import Architecture
from wavefier_ml.preprocessing.Scaling import Scaling
from wavefier_common.TriggerCollection import TriggerCollection
from wavefier_common.util.Path import Path

import os
import random
import numpy as np
import json
import logging

from matplotlib import use
use("Agg")
import matplotlib.pyplot as plt
from sklearn.preprocessing import OneHotEncoder
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split


class Model:
    """
    The CNN1D class based on Keras implementation.
    """
    def __init__(self, triggers: TriggerCollection):
        """
        The CNN1D class based on Keras implementation.

        :type trigger: TriggerCollection
        :param trigger: Set of triggers to train the CNN1D
        """
        # Prepare data for training
        data = np.zeros((triggers.count(), triggers.getWDFObject().Ncoeff, 1))
        logging.info("Preparing the data for training")
        j = 0
        self.labels = np.zeros((triggers.count(), 1), dtype=int)
        for trigger in triggers.iterator():
            coefficients = np.zeros((1, trigger.getNcoef()), dtype=float)
            for i in range(trigger.getNcoef()):
                coefficients[0, i] = trigger.getRW(i)
            normed_coefficients = Scaling.normalize(coefficients)
            #data.append(normed_coefficients)
            data[j,:] = normed_coefficients.reshape(normed_coefficients.shape[0], normed_coefficients.shape[1], 1)
            self.labels[j,:] = trigger.label
            j+=1
        self.data = np.array(data)

        # Load configuration
        logging.info("Loading configuration")
        script_dir = os.path.abspath(os.path.dirname(__file__))
        config_json = str(Path(script_dir) / 'config.json')
        with open(config_json, "r") as f:
            self.config = json.load(f)

        self.architecture = self.config["architecture"]
        self.loss = self.config["loss"]
        self.hyper_params = self.config["hyper_params"]

        # Directory for the output model
        self.model_output = os.path.join(script_dir, 'models')
        if not os.path.exists(self.model_output):
            os.makedirs(self.model_output)


    def fit(self):
        """
        The fit module builds and trains the CNN1D according to the set of hyperparameters and architecture.
        """

        # basic information about the data
        val_ratio = self.hyper_params["val_ratio"]


        # Convert label to one hot encoded representation
        onehot_encoder = OneHotEncoder(sparse=False)
        encoded_classes = onehot_encoder.fit_transform(self.labels)


        # Split the data for the training
        x_train, x_val, y_train, y_val = train_test_split(self.data, encoded_classes, test_size=val_ratio, random_state=42)

        # hyperparameters for optimizer
        max_iter = self.hyper_params["max_iter"]
        batch_size = self.hyper_params["batch_size"]

        # Build the model
        logging.info("Building the model")
        cnn1d = Architecture(self.data.shape, self.architecture["network"], self.loss, self.hyper_params["n_classes"])
        cnn1d.build()

        # Train the model
        logging.info("Training the model")
        history = cnn1d.classifier.fit(x_train, y_train,
                        epochs=max_iter,
                        batch_size=batch_size,
                        shuffle=True,
                        validation_data=(x_val, y_val)
        )

        # Saves the learning history
        logging.info("Saving the training history to the file")
        output = np.array([history.history["loss"], history.history["val_loss"]])
        output = output.reshape(output.shape[1], output.shape[0])
        with open(self.model_output + "/history_cnn1d.dat", "ab") as f:
            np.savetxt(f, output)

        # Plots the training history
        logging.info("Plotting the training history")
        fig = plt.figure(figsize=(16, 14))
        plt.plot(output[:, 0], 'green', label="train_loss: %.5f" % output[-1, 0])
        plt.plot(output[:, 1], label="val_loss: %.5f" % output[-1, 1])
        plt.xlabel("Epoch")
        plt.legend()
        plt.savefig(self.model_output + "/history_cnn1d.png")

        # Plots the confusion matrix
        cms = confusion_matrix(y_val.argmax(1), y_val.argmax(1))
        test_score = np.trace(cms) / np.sum(cms)
        fig = plt.figure(figsize=(18, 14))
        ax = fig.add_subplot(111)
        im = ax.imshow(np.transpose(cms), interpolation="nearest", cmap="cool")
        rows = cms.shape[0]
        cols = cms.shape[1]
        for x in range(0, rows):
            for y in range(0, cols):
                value = int(cms[x, y])
                ax.text(x, y, value, color="black", ha="center", va="center", fontsize=25)
        plt.title("Real vs predicted data, accuracy: " + str(test_score), fontsize=25)
        plt.colorbar(im)

        classes_values = []
        classes_labels = []
        for n in range(self.hyper_params["n_classes"]):
            classes_values.append(n)
            classes_labels.append(str(n))

        plt.xticks(classes_values, classes_labels, rotation=45, fontsize=25)
        plt.yticks(classes_values, classes_labels, fontsize=25)
        plt.xlabel("Real data", fontsize=25)
        plt.ylabel("Predicted data", fontsize=25)
        plt.savefig(self.model_output + "/cm_cnn1d.png")

        # serialize model to JSON
        logging.info("Save CNN1D to the file")
        model_json = cnn1d.classifier.to_json()
        with open(self.model_output + "/model.json", "w") as json_file:
            json_file.write(model_json)
        # serialize weights to HDF5
        cnn1d.classifier.save_weights(self.model_output + "/model.hdf5")
        logging.info("Saved model to disk")
