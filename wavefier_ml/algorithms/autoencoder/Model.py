from wavefier_ml.algorithms.autoencoder.Architecture import Architecture
from wavefier_ml.preprocessing.Scaling import Scaling
from wavefier_common.TriggerCollection import TriggerCollection
from wavefier_common.util.Path import Path

import os
import random
import numpy as np
import json
import logging

from matplotlib import use
use("Agg")
import matplotlib.pyplot as plt


class Model:
    """
    The Autoencoder class based on Keras implementation.
    """
    def __init__(self, triggers: TriggerCollection):
        """
        The Autoencoder class based on Keras implementation.

        :type trigger: TriggerCollection
        :param trigger: Set of triggers to train the Autoencoder
        """
        # Prepare data for training
        data = np.zeros((triggers.count(), triggers.getWDFObject().Ncoeff))
        logging.info("Preparing the data for training")
        j = 0
        for trigger in triggers.iterator():
            coefficients = np.zeros((1, trigger.getNcoef()), dtype=float)
            for i in range(trigger.getNcoef()):
                coefficients[0, i] = trigger.getRW(i)
            normed_coefficients = Scaling.normalize(coefficients)
            #data.append(normed_coefficients)
            data[j,:] = normed_coefficients
            j+=1
        self.data = np.array(data)

        # Load configuration
        logging.info("Loading configuration")
        script_dir = os.path.abspath(os.path.dirname(__file__))
        config_json = str(Path(script_dir) / 'config.json')
        with open(config_json, "r") as f:
            self.config = json.load(f)

        self.architecture = self.config["architecture"]
        self.loss = self.config["loss"]
        self.hyper_params = self.config["hyper_params"]

        # Directory for the output model
        self.model_output = os.path.join(script_dir, 'models')
        if not os.path.exists(self.model_output):
            os.makedirs(self.model_output)


    def fit(self):
        """
        The fit module builds and trains the Autoencoder according to the set of hyperparameters and architecture.
        """

        # basic information about the data
        n_samples, n_features = self.data.shape
        val_ratio = self.hyper_params["val_ratio"]

        val_samples = int(val_ratio*n_samples)
        self.train_samples = n_samples - val_samples

        # Split the data
        x_train = self.data[:self.train_samples, :]
        x_val = self.data[self.train_samples:, :]

        # hyperparameters for optimizer
        max_iter = self.hyper_params["max_iter"]
        batch_size = self.hyper_params["batch_size"]

        # Build the model
        logging.info("Building the model")
        self.ae = Architecture(n_features, self.architecture["network"], self.loss)
        self.ae.build()

        # Train the model
        logging.info("Training the model")
        history = self.ae.autoencoder.fit(x_train, x_train,
                        epochs=max_iter,
                        batch_size=batch_size,
                        shuffle=True,
                        validation_data=(x_val, x_val)
                           )
        # Saves the learning history
        logging.info("Saving the training history to the file")
        output = np.array([history.history["loss"], history.history["val_loss"]])
        output = output.reshape(output.shape[1], output.shape[0])
        with open(self.model_output + "/history_ae.dat", "ab") as f:
            np.savetxt(f, output)

        # Plots the training history
        logging.info("Plotting the training history")
        fig = plt.figure(figsize=(16, 14))
        plt.plot(output[:, 0], 'green', label="train_loss: %.5f" % output[-1, 0])
        plt.plot(output[:, 1], label="val_loss: %.5f" % output[-1, 1])
        plt.xlabel("Epoch")
        plt.legend()
        plt.savefig(self.model_output + "/history_ae.png")

        # serialize model to JSON
        logging.info("Save autoencoder to the file")
        model_json = self.ae.autoencoder.to_json()
        with open(self.model_output + "/model.json", "w") as json_file:
            json_file.write(model_json)
        # serialize weights to HDF5
        self.ae.autoencoder.save_weights(self.model_output + "/model.hdf5")
        logging.info("Saved model to disk")

        # serialize encoder to JSON
        logging.info("Save encoder to the file")
        model_json = self.ae.encoder.to_json()
        with open(self.model_output + "/model_encoder.json", "w") as json_file:
            json_file.write(model_json)
        # serialize weights to HDF5
        self.ae.encoder.save_weights(self.model_output + "/model_encoder.hdf5")
        logging.info("Saved model to disk")