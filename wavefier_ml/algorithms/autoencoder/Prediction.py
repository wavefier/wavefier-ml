from wavefier_common.util.Path import Path
from wavefier_common.TriggerCollection import TriggerCollection
from wavefier_common.Trigger import Trigger
from wavefier_common.TriggerUnsupervisedResults import TriggerUnsupervisedResults
from wavefier_ml.preprocessing.Scaling import Scaling

from keras.models import model_from_json

import numpy as np
import os
import json
import logging

class Prediction:
    """
    This class uses precomputed Autoencoder to convert the data into latent space (less dimensional).
    """
    @staticmethod
    def predict(trigger: Trigger):
        """
        Static method converting single trigger into its latent space representation

        :type trigger: Trigger
        :param trigger: Single Trigger object
        :return: Trigger Cluster object with model configuration and the encoded trigger
        """
        logging.info("Loading the model")
        script_dir = os.path.abspath(os.path.dirname(__file__))
        subdir = os.path.join(script_dir, 'models')
        json_path = str(Path(subdir) / 'model_encoder.json')
        hdf_path = str(Path(subdir) / 'model_encoder.hdf5')

        # load json and create model
        json_file = open(json_path, 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        loaded_model = model_from_json(loaded_model_json)
        # load weights into new model
        loaded_model.load_weights(hdf_path)
        logging.info("Model loaded from disk")

        logging.info("Running the autoencoder")
        coefficients = np.zeros((1, trigger.getNcoef()), dtype=float)
        for i in range(trigger.getNcoef()):
            coefficients[0, i] = trigger.getRW(i)
        normed_coefficients = Scaling.normalize(coefficients)
        encoded_img = loaded_model.predict(normed_coefficients)
        logging.info("Data processed")
        
        # Store the results as Trigger Cluster object
        trigger_cluster = TriggerUnsupervisedResults(trigger.uuid)
        # Load the algorithm configuration
        logging.info("Loading configuration")
        script_dir = os.path.abspath(os.path.dirname(__file__))
        config_json = str(Path(script_dir) / 'config.json')
        with open(config_json, "r") as f:
            config = json.load(f)
        trigger_cluster.setAlgorithmConfig("autoencoder", config)
        trigger_cluster.setMLResults("autoencoder", encoded_img)
        return trigger_cluster


    @staticmethod
    def predict_all(triggers: TriggerCollection):
        """
        Deprecated method
        Static method converting set of triggers into their latent space representation

        :type trigger: TriggerCollection
        :param trigger: Set of Triggers
        :return: List of Trigger Cluster objects with model configuration and the encoded trigger
        """
        logging.info("Loading the model")
        script_dir = os.path.abspath(os.path.dirname(__file__))
        subdir = os.path.join(script_dir, 'models')
        json_path = str(Path(subdir) / 'model_encoder.json')
        hdf_path = str(Path(subdir) / 'model_encoder.hdf5')
        
        # load json and create model
        json_file = open(json_path, 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        loaded_model = model_from_json(loaded_model_json)
        # load weights into new model
        loaded_model.load_weights(hdf_path)
        logging.info("Model loaded from disk")

        # Load algorithm configuration
        logging.info("Loading configuration")
        script_dir = os.path.abspath(os.path.dirname(__file__))
        config_json = str(Path(script_dir) / 'config.json')
        with open(config_json, "r") as f:
            config = json.load(f)

        logging.info("Running the autoencoder")
        encoded_all_triggers = []
        for trigger in triggers.iterator():
            coefficients = np.zeros((1, trigger.getNcoef()), dtype=float)
            for i in range(trigger.getNcoef()):
                coefficients[0,i] = trigger.getRW(i)
            normed_coefficients = Scaling.normalize(coefficients)
            encoded_img = loaded_model.predict(normed_coefficients)

            # Store the results as Trigger Classify object
            trigger_classifier = TriggerUnsupervisedResults(trigger.uuid)
            trigger_classifier.setAlgorithmConfig("cnn1d", config)
            trigger_classifier.setMLResults("cnn1d", encoded_img)
            encoded_all_triggers.append(trigger_classifier)

        logging.info("Data processed")
        return encoded_all_triggers

