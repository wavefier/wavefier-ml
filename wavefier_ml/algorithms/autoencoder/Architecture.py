from tensorflow.keras.layers import Input, Dense, Dropout
from tensorflow.keras.models import Model
from tensorflow.keras import optimizers

class DenseLayer:
    """
    The DenseLayer class is responsible for the initialization of each layer in the architecture of the Autoencoder.
    """
    def __init__(self, n_neurons: int, f: str, dropout: float):
        """
        The DenseLayer class is responsible for the initialization of each layer in the architecture of the Autoencoder.

        :type n_neurons: int
        :param n_neurons: Integer specifying the number of neurons in the dense layer.

        :type f: string
        :param f: Name of the activation function for the layer.

        :type dropout: float
        :param dropout: The value of dropout - fraction of neurons to be turned of during the training. If dropout=0.0
        then no Dropout is applied to the layer.
        """
        self.f = f
        self.n_neurons = n_neurons
        self.dropout = dropout

    def forward(self, X: Input):
        """
        The forward module initialize the layer with respect to the given set of parameters. If available,
        it applied dropout.

        :type X: keras layer
        :param X: The keras layer object passed from the previous layers or the input layer.
        """
        output =  Dense(self.n_neurons, activation=self.f)(X)
        if self.dropout > 0.0:
            output = Dropout(self.dropout)(output)
        return output


class Architecture:
    """
    The Architecture class builds the Fully connected Autoencoder based on the given structure and hyperparameters.
    """
    def __init__(self, n_inputs: int, layers: dict, loss: str):
        """
        The Architecture class builds the Fully connected Autoencoder based on the given structure and hyperparameters.

        :type n_inputs: int
        :param n_inputs: Number of features in the dataset

        :type layers: dict
        :param layers: Dictionary containing lists of parameters for each layer of the Autoencoder

        :type loss: str
        :param loss: Name of the loss function
        """
        self.layers = layers["dense"]
        self.n_inputs = n_inputs
        self.lr = loss["lr"]
        self.optimizer = loss["optimizer"]
        self.loss = loss["cost"]
        self.activ = loss["activation"]
        self.inputs = Input(shape=(n_inputs,))

    def build(self):
        """
        The build module is responsible for the construction of the whole architecture for the Autoencoder based on
        the configuration file.
        """
        # Encoder
        encoder_layers = []
        for n_neurons, dropout in self.layers:
            h = DenseLayer(n_neurons, self.activ, dropout)
            encoder_layers.append(h)

        # Separate case for encoder's finala layer in case of modifications
        #n_neurons, dropout = self.layers[-1]
        #h = DenseLayer(n_neurons, self.activ, dropout)
        #encoder_layers.append(h)

        # Process
        current_layer_value = self.inputs
        for layer in encoder_layers:
            current_layer_value = layer.forward(current_layer_value)
        encoded_layer = current_layer_value

        # Decoder
        decoder_layers = []
        for n_neurons, dropout in reversed(self.layers[:-1]):
            h = DenseLayer(n_neurons, self.activ, dropout)
            decoder_layers.append(h)
        # Decoder final layer should go through a sigmoid
        h = DenseLayer(self.n_inputs, self.activ, 0.0)
        decoder_layers.append(h)

        for layer in decoder_layers:
            current_layer_value = layer.forward(current_layer_value)
        decoded_layer = current_layer_value

        # Optimizer initialization
        opt_to_use = getattr(optimizers, self.optimizer)
        optimizer = opt_to_use(lr=self.lr)

        # Create containers for all models
        # Main model
        self.autoencoder = Model(self.inputs, decoded_layer)
        # Model to visualize latent space
        self.encoder = Model(self.inputs, encoded_layer)

        # Compile the model
        self.autoencoder.compile(loss=self.loss, optimizer=optimizer)
