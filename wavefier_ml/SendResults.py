from wavefier_common.kafka.avro.Producer import Producer
from wavefier_common.kafka.avro.Message import Message
from wavefier_common.TriggerUnsupervisedResults import TriggerUnsupervisedResults
from wavefier_common.TriggerSupervisedResults import TriggerSupervisedResults


from wavefier_ml.db.MLResults2DBImporter import MLResults2DBImporter

import logging
import os
import wavefier_ml

class SendResults:
    """
    This class is responsible for the communication with kafka and the DB to send the results of analysis
    """
    def __init__(self):
        """
            This class is responsible for the communication with the Kafka and DB to send the results of analysis
        """
        #TODO REMOVE ABSOLU!!!!
        db_host = os.environ['DB_HOST'] 
        db_port = os.environ["DB_PORT"]
        db_name = os.environ["DB_NAME"]
        broker = os.environ['KAFKA_BROKER']
        registry = os.environ["KAFKA_REGISTRY"]

        logging.info("Creating the DB Importer on:  " + str(db_host) + ", at the port: " + str(db_port) + " Database: "+ str(db_name))
        self.db = MLResults2DBImporter(db_host, db_port, db_name)

        broker_parts = broker.split(":")
        broker_host = broker_parts[0]
        broker_port = broker_parts[1]

        # Connecting to the kafka broker
        logging.info("Connecting to the kafka broker: " + str(broker_host) + ", at the port: " + str(broker_port))
        conf = {'bootstrap.servers': broker,
                'schema.registry.url': registry}

        self.producer = Producer(conf)

    def send(self, trigger, result):
        """
        Method sending results of analysis to the DB
        :return:
        :type result: TriggerSupervisedResults or TriggerUnsupervisedResults
        :param result: Object containing results of analysis
        """
        logging.info("Sending results")

        if result is not None and isinstance(result, TriggerUnsupervisedResults) :
            messageEnv = Message(wavefier_ml.DEFAULT_UnsupervisedML_TOPIC_NAME, object=result)
            #self._send_on_db(result)  Need to understand the result before to save it
            #self._send_on_kafka(messageEnv)

        elif result is not None and isinstance(result, TriggerSupervisedResults) :
            self._send_on_db(trigger, result)

            messageEnv = Message(wavefier_ml.DEFAULT_SupervisedML_TOPIC_NAME, object=result)
            self._send_on_kafka(messageEnv)
        else:
            logger.warning("Result not recognised, IGNORED!")

    def _send_on_db(self, trigger, result):
        logging.debug("New ML result generated: "+ str(result))
        logging.debug("Send ML result on DB" )
        responce = "KO"
        try:
            responce = self.db.newResultsAppears(trigger,result)
        except:
            logging.exception("SOME ERROR THERE")
            
        logging.info("Send responser: " + str(responce))               

    def _send_on_kafka(self, messageEnv):
        logging.debug("Send Trigger on kafka" )
        response = "KO"
        try:
            response = self.producer.sync_delivery(messageEnv)
        except:
            logging.exception("SOME ERROR THERE")

        logging.info("Status of the kafka message sent: " + str(response))           
