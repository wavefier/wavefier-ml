from wavefier_common.Trigger import Trigger
from wavefier_ml.TriggerAnalysis import TriggerAnalysis
from wavefier_ml.SendResults import SendResults

import logging
logger = logging.getLogger(__name__)

class RunMLAnalysis:
    """
    Main class of the wavefier-ml - applies chosen machine learning method to the triggers list

    Current version applies:
    - autoencoder as a method of clusterization
    - CNN 1D as a method of classification

    """
    def __init__(self, trigger: Trigger, mode: str = "classify"):
        """
        Main class of the wavefier-ml - applies chosen machine learning method to the triggers list

        :type triggers: Trigger
        :param triggers: Trigger object containing trigger to analyse

        :type mode: str
        :param mode: Parameter specifying type of analysis
        """
        self.trigger = trigger
        self.mode = mode


    def run(self):

        """
        Method calling other APIs to process data

        :return:
        """
        # Run triggers analysis
        logger.info("[Trigger: " + self.trigger.uuid +"] Run "+ self.mode +" analysis..." )
        ta = TriggerAnalysis(self.trigger, self.mode)
        analysis_output = ta.run()

        logger.info("[Trigger: " + self.trigger.uuid +"] ... result:" + str(analysis_output ))

        # Send the results of analysis further
        sr = SendResults()
        sr.send(self.trigger,analysis_output)
