import logging
import os

from wavefier_ml.MLListener import MLListener
from wavefier_trigger_handlers.TriggerSupplier import TriggerSupplier

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

if __name__ == "__main__":
    
    broker = os.environ['KAFKA_BROKER']
    registry = os.environ["KAFKA_REGISTRY"]

    broker_parts = broker.split(":")
    registry_parts = registry.split(":")

    broker_host = broker_parts[0]
    broker_port = broker_parts[1]

    registry_host = registry[:-len(registry_parts[-1])-1]
    registry_port = registry_parts[-1]

    logger.info("Connecting for the listening of trigger to the kafka broker : " + str(broker_host) + ", at the port: " + str(broker_port))
    sup = TriggerSupplier(broker_host, broker_port, registry_host, registry_port)
    sup.addTriggerListener(MLListener())
    sup.startObserving()
