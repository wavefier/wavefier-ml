from setuptools import setup, find_packages

import wavefier_ml

setup(
    name='wavefier_ml',
    author="Filip",
    version=wavefier_ml.__version__,
    description='Wavefier ML - Machine Learning library for the Wavefier project',
    url='',
    author_email='filipmor92@gmail.com',
    packages=find_packages(),
    package_data = {
         'wavefier_ml.algorithms.autoencoder': ['*'],
         'wavefier_ml.algorithms.cnn1d': ['*']
    },
    test_suite='nose.collector',
    setup_requires=['nose>=1.0'],
    install_requires=[ 
        'wavefier-common==2.0.0',
        'wavefier-trigger-handlers==2.0.0', 
        'influxdb'
    ],
    dependency_links=[
        "git+https://gitlab+deploy-token-576329:xEesa1mjDRs1dt2KaeH7@gitlab.com/wavefier2021/wavefier-common.git#egg=wavefier-common-2.0.0",
        "git+https://gitlab+deploy-token-576329:xEesa1mjDRs1dt2KaeH7@gitlab.com/wavefier2021/wavefier-trigger-handlers.git#egg=wavefier-trigger-handlers-2.0.0",
   ]
)
