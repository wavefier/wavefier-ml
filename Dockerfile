FROM wdfteam/wdfpipe:2.0.0


RUN mkdir /wavefier-ml

WORKDIR /wavefier-ml

COPY .  /wavefier-ml

RUN apt-get --force-yes update
#RUN apt-get --assume-yes install librdkafka1
RUN apt-get --assume-yes install librdkafka-dev

RUN pip install tensorflow keras matplotlib

RUN python setup.py install

RUN pip uninstall -y wavefier-trigger-handlers

RUN python setup.py develop

RUN python tools/prepare_cnn1d.py

RUN python tools/prepare_ae.py

CMD ["python", "-u", "wavefier_ml/WatcherTriggers.py"]
