from unittest import TestCase
import uuid
import os

from wavefier_common.util.Path import Path
from wavefier_common.param.WitheningParam import WitheningParam
from wavefier_common.param.WdfParam import WdfParam

from wavefier_import_handlers.RawSupplier import RawSupplier

from wavefier_trigger_handlers.TriggerAdapter import TriggerAdapter
from wavefier_trigger_handlers.TriggerDistributor import TriggerDistributor

from wavefier_ml.MLListener import MLListener

broker = "kafka_test:9092"
os.environ['KAFKA_BROKER'] = broker

broker_parts = broker.split(":")
broker_host = broker_parts[0]
broker_port = broker_parts[1]

class TestUtil():
    @staticmethod
    def sendMessage():
        wdf_parameters = WdfParam(1024, 16, 4)
        w_parameters = WitheningParam(3000)
        channel = "V1:Hrec_hoft_16384Hz_O2Replay2"
        script_dir = os.path.abspath(os.path.dirname(__file__))
        subdir = os.path.join(script_dir, 'test_static_data')
        file_path = str(Path(subdir) / 'test_csv')
        triggers = TriggerAdapter.fromCSV(
            file_path,
            channel,
            w_parameters,
            wdf_parameters
        )

        dist = TriggerDistributor(broker_host, broker_port)

        for trigger in triggers.iterator():
            unique_id = str(uuid.uuid4())
            trigger.__setattr__("uuid", unique_id)
            dist.deploy(channel, w_parameters, wdf_parameters, trigger)



class TestMLListener(TestCase):

    def test_listener(self):

        # Send data to kafka
        TestUtil.sendMessage()

        # Run ML listener
        sup = RawSupplier(broker)
        sup.addRawListener(MLListener())
        sup.startObserving()
        sup.stopObserving()

        sup.join()