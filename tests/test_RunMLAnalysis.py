from unittest import TestCase
import os
import uuid
import numpy as np

from wavefier_common.util.Path import Path
from wavefier_common.param.WitheningParam import WitheningParam
from wavefier_common.param.WdfParam import WdfParam

from wavefier_trigger_handlers.TriggerAdapter import TriggerAdapter

from wavefier_ml.RunMLAnalysis import RunMLAnalysis
from wavefier_ml.TrainModel import TrainModel

class TestRunMLAnalysis(TestCase):

    def test_training_prediction(self):

        # Train the model
        script_dir = os.path.abspath(os.path.dirname(__file__))
        subdir = os.path.join(script_dir, 'test_data')
        file_path = str(Path(subdir) / 'test_csv')
        triggers = TriggerAdapter.fromCSV(
            file_path,
            "V1:Hrec_hoft_16384Hz_O2Replay2",
            WitheningParam(3000),
            WdfParam(1024, 16, 4)
        )

        # Create random labels for triggers
        n_classes = 5
        classes = np.random.randint(0, n_classes, triggers.count())
        i = 0
        for trigger in triggers.iterator():
            trigger.__setattr__("label", classes[i])
            # print(trigger.label)
            i += 1
        mla = TrainModel(triggers, mode="classify")
        mla.run()

        # Extract the data
        script_dir = os.path.abspath(os.path.dirname(__file__))
        subdir = os.path.join(script_dir, 'test_static_data')
        file_path = str(Path(subdir) / 'test_csv')
        triggers = TriggerAdapter.fromCSV(
            file_path,
            "V1:Hrec_hoft_16384Hz_O2Replay2",
            WitheningParam(3000),
            WdfParam(1024, 16, 4)
        )

        # Test the prediction
        for trigger in triggers.iterator():
            unique_id = str(uuid.uuid4())
            trigger.__setattr__("uuid", unique_id)
            mla = RunMLAnalysis(trigger)
            self.assertIsNone(mla.run())
