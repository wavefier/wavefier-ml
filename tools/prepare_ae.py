from wavefier_ml.TrainModel import TrainModel
from wavefier_trigger_handlers.TriggerAdapter import TriggerAdapter
from wavefier_common.util.Path import Path
from wavefier_common.param.WitheningParam import WitheningParam
from wavefier_common.param.WdfParam import WdfParam

import os

if __name__ == "__main__":
    script_dir = os.path.abspath(os.path.dirname(__file__))
    subdir = os.path.join(script_dir, 'training_data')
    file_path = str(Path(subdir) / 'data.csv')
    triggers = TriggerAdapter.fromCSV(
        file_path,
        "V1:Hrec_hoft_16384Hz",
        WitheningParam(3000),
        WdfParam(1024, 16, 4)
    )
    mla = TrainModel(triggers, mode="cluster")
    mla.run()
