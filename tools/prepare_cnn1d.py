from wavefier_ml.TrainModel import TrainModel
from wavefier_trigger_handlers.TriggerAdapter import TriggerAdapter
from wavefier_common.util.Path import Path
from wavefier_common.param.WitheningParam import WitheningParam
from wavefier_common.param.WdfParam import WdfParam

import os
import numpy as np

if __name__ == "__main__":
    script_dir = os.path.abspath(os.path.dirname(__file__))
    subdir = os.path.join(script_dir, 'training_data')
    file_path = str(Path(subdir) / 'data.csv')
    triggers = TriggerAdapter.fromCSV(
        file_path,
        "V1:Hrec_hoft_16384Hz",
        WitheningParam(3000),
        WdfParam(1024, 16, 4)
    )

    # Create random labels for triggers
    n_classes = 5
    classes = np.random.randint(0, n_classes, triggers.count())
    i = 0
    for trigger in triggers.iterator():
        trigger.__setattr__("label", classes[i])
        i += 1
    mla = TrainModel(triggers, mode = "classify")
    mla.run()
