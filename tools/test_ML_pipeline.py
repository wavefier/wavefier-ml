from wavefier_ml.RunMLAnalysis import RunMLAnalysis
from wavefier_trigger_handlers.TriggerAdapter import TriggerAdapter
from wavefier_common.util.Path import Path
from wavefier_common.param.WitheningParam import WitheningParam
from wavefier_common.param.WdfParam import WdfParam

import os

if __name__ == "__main__":
    script_dir = os.path.abspath(os.path.dirname(__file__))
    subdir = os.path.join(script_dir, 'test_data')
    file_path = str(Path(subdir) / 'test.csv')
    triggers = TriggerAdapter.fromCSV(
        file_path,
        "V1:Hrec_hoft_16384Hz",
        WitheningParam(3000),
        WdfParam(1024, 16, 4)
    )
    for trigger in triggers.iterator():
        mla = RunMLAnalysis(trigger)
        mla.run()
        break