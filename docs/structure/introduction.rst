Introduction
=============

The library contains the following machine learning algorithms:
- AutoEncoders
- 1 Dimensional Convolutional Neural Networks

Requirements
-------------

- wavefier-common
- wavefier-import-handler
- wavefier-trigger-handlers
- tensorflow
- keras

Installation
-------------

To install the wavefier-ml library, one has to run `setup.py` script from the main directory of the library.

``python setup.py install``
