Tutorial
=====================

From the main directory of the library, open `tests` folder. There you will find few examples of how the code works.
Currently there are 3 scripts:

* test_autoencoder.py - tests the whole library by running RunMLAnalysis.py script on the sample data from `test_data` folder

To run the script, write in the terminal, e.g.:

``python test_autoencoder.py``

On the screen, you will set of logs and informations regarding various steps of processing the data.