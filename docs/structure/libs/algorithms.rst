The Machine Learning algorithms
==================================


AutoEncoder
-------------

.. automodule:: algorithms.autoencoder.Model
   :members:

.. automodule:: algorithms.autoencoder.Architecture
   :members:

.. automodule:: algorithms.autoencoder.Prediction
   :members:


CNN1D
-----------



.. automodule:: algorithms.cnn1d.Model
   :members:

.. automodule:: algorithms.cnn1d.Architecture
   :members:

.. automodule:: algorithms.cnn1d.Prediction
   :members:
