List of APIs
===============


RunMLAnalysis
---------------

.. automodule:: RunMLAnalysis
   :members:

TriggerAnalysis
-----------------

.. automodule:: TriggerAnalysis
   :members:


SendResults
------------

.. automodule:: SendResults
   :members:

MLListener
-----------

.. automodule:: MLListener
   :members:

TrainModel
------------

.. automodule:: TrainModel
   :members:


WatcherTriggers
------------

.. automodule:: WatcherTriggers
   :members:


MLListener
------------

.. automodule:: MLListener
   :members:


