The library's code
=======================

The main directory contains set of APIs responsible for the communication with various parts of the library.
The **algorithms** directory contains set of Machine Learning algorithms used to process data either in supervised or
unsupervised approach.
The **preprocessing** directory contains classes associated with the rescaling of the data.
The list of APIs:
* `RunMLAnalysis` - main API of the wavefier-ml; it calls all the others APIs to process the data and send it to the database
* `TrigerAnalysis` - API responsible for the application of chosen Machine Learning algorithm to the set of triggers
* `SendResults` - API sending the results of Machine Learning analysis to the database


Library's content
-------------------

.. toctree::
   :maxdepth: 5

   libs/apis.rst
   libs/algorithms.rst
   libs/preprocessing.rst
